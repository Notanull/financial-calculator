package com.navneetgupta.domain.jsonprotocols

import com.navneetgupta.infrastructure.jsonprotocols.BaseJsonProtocol
import com.navneetgupta.domain.model._

trait CalculatorJsonProtocol extends BaseJsonProtocol {
  implicit val valueFormat = jsonFormat1(Value.apply)
  implicit val schedulesFormat = jsonFormat4(Schedules.apply)
  implicit val inputModelFormat = jsonFormat3(InputModel.apply)
  implicit val responseFormat = jsonFormat2(ResponseModel.apply)
}
