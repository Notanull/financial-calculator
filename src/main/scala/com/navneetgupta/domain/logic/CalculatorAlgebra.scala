package com.navneetgupta.domain.logic

import com.navneetgupta.domain.model.CalculatorModel
import com.navneetgupta.domain.model.PayBack
import scala.annotation.tailrec

/*
 * Contains Algorithm with Relevant function to calculate the Rates for Calculator
 *
 */

final class CalculatorAlgebra(f: Double => Double,
                              df: Double => Double = identity) {
  final val ACCURACY = 0.000001

  /**
   * Using Newton-Rapson Method for Finding Roots
   * Takes two parameter function(f) for which we need to find the root and the first derivative(df) of that same function
   * Since its Tail recursive optimized there is no use of Stack to maintain recursion state
   * This is used for finding the rates
   */
  @tailrec
  def newtonsMethod(rateToTry: Double): Double = {
    val nextRateToTry = rateToTry - f(rateToTry) / df(rateToTry)
    if (Math.abs(nextRateToTry - rateToTry) < ACCURACY) nextRateToTry
    else newtonsMethod(nextRateToTry)
  }

  /**
   * Anohter Approximate method to find the IRR/APR using approximation
   */
  def getUsingApproximateMethod(_rateToTry: Double): Double = {
    @tailrec
    def calculate(rateToTry: Double = 0.0, difference: Double = 1.0, amountToAdd: Double = 0.0001): Double = {
      f(rateToTry) match {
        case x if (Math.abs(x) < ACCURACY) => rateToTry
        case x if (x > 0)                  => calculate(rateToTry + amountToAdd * 2, x, amountToAdd * 2)
        case x if (x < 0)                  => calculate(rateToTry - amountToAdd / 2, x, amountToAdd / 2)
      }
    }
    calculate(_rateToTry)
  }
}
