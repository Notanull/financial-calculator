package com.navneetgupta.domain.main

import com.typesafe.config.ConfigFactory
import akka.actor._
import akka.stream.ActorMaterializer
import akka.event.Logging
import com.navneetgupta.infrastructure.boot.Server

object CalculatorMain extends App {
  new Server(new CalculatorBootstrap(), "calculator")
}
