package com.navneetgupta.domain.model

case class CalculatorModel(advance: Double, installments: List[PayBack])

trait Algebra[A, B] {
  def calculate(value: A, x: Double): Double
  def calculateDerivative(value: A, x: Double): Double
  def presentValue(value: A, x: Double, f: (Double, B) => Double): Double
}

object AlgebraInstances {
  implicit val calculatorModelAlgebra: Algebra[CalculatorModel, PayBack] =
    new Algebra[CalculatorModel, PayBack] {
      override def calculate(value: CalculatorModel, x: Double) = {
        val repaymentPresentValueAtXRate = value.installments.foldLeft(0.0)((a, b) => a + b.amount / Math.pow(1 + x, b.period))
        repaymentPresentValueAtXRate - value.advance
      }

      override def calculateDerivative(value: CalculatorModel, x: Double) = {
        value.installments.foldLeft(0.0)((a, b) => a - (b.period) * b.amount / Math.pow(1 + x, b.period + 1))
      }

      override def presentValue(value: CalculatorModel, x: Double, f: (Double, PayBack) => Double): Double = {
        val payments = value.installments.foldLeft(0.0)((a, b) => a + f(x, b))
        payments - value.advance
      }
    }
}

object CalculatorModel {
  implicit class AlgebraOps[CalculatorModel](value: CalculatorModel) {
    def calculate(x: Double)(implicit a: Algebra[CalculatorModel, PayBack]) = {
      a.calculate(value, x)
    }
    def calculateDerivative(x: Double)(implicit a: Algebra[CalculatorModel, PayBack]) = {
      a.calculateDerivative(value, x)
    }
    def presentValue(x: Double, f: (Double, PayBack) => Double)(implicit a: Algebra[CalculatorModel, PayBack]) = {
      a.presentValue(value, x, f)
    }
  }
}
