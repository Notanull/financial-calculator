package com.navneetgupta.infrastructure.boot

import akka.actor.ActorSystem
import com.navneetgupta.infrastructure.routes.BaseRouteDefination

trait Bootstrap {
  def bootstrap(system: ActorSystem): List[BaseRouteDefination]
}
