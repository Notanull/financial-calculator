package com.navneetgupta.infrastructure.boot

import com.typesafe.config.ConfigFactory
import akka.stream.ActorMaterializer
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.scaladsl.Sink
import akka.event.Logging

class Server(boot: Bootstrap, service: String) {
  import akka.http.scaladsl.server.Directives._
  val conf = ConfigFactory.load()

  implicit val system = ActorSystem(service, conf)
  implicit val mater = ActorMaterializer()
  val log = Logging(system.eventStream, "Server")
  import system.dispatcher

  val routes = boot.bootstrap(system).map(_.routes)
  val definedRoutes = routes.reduce(_ ~ _)
  val finalRoutes = pathPrefix("calculator")(definedRoutes)

  val conf1 = ConfigFactory.load.getConfig(service.toLowerCase()).resolve()

  val serviceConf = system.settings.config.getConfig(service.toLowerCase())

  val serverSource =
    Http().bind(interface = serviceConf.getString("ip"), port = serviceConf.getInt("port"))

  log.info("Starting up on port {} and ip {}", serviceConf.getString("port"), serviceConf.getString("ip"))

  val sink = Sink.foreach[Http.IncomingConnection](_.handleWith(finalRoutes))
  serverSource.to(sink).run
}
