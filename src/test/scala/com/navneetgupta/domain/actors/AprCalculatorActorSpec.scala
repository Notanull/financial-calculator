package com.navneetgupta.domain.actors

import akka.testkit.TestKit
import akka.actor.ActorSystem
import com.navneetgupta.infrastructure.StopSystemAfterAll
import akka.testkit.ImplicitSender
import akka.actor.Props
import akka.testkit.TestProbe
import com.navneetgupta.domain.command.CalculateRate
import com.navneetgupta.domain.model._
import com.navneetgupta.domain.events.APRCalculated
import org.scalatest.FunSuiteLike

class AprCalculatorActorSpec extends TestKit(ActorSystem("calculator-test"))
    with FunSuiteLike
    with ImplicitSender
    with StopSystemAfterAll {

  import CalculatorModel._
  import AlgebraInstances._

  val aprCalculator = system.actorOf(Props[AprCalculatorActor], "aprCalculator")

  test("AprCalculator should handle Command CalculateAPR using testProbe") {
    val sender = TestProbe()
    sender.send(aprCalculator, CalculateRate(CalculatorModel(100, List(PayBack(101, 12)))))
    val state = sender.expectMsgType[APRCalculated]
  }

  test("AprCalculator should calculated APR Should be rounded to one Decimal place using testProbe") {
    val sender = TestProbe()
    sender.send(aprCalculator, CalculateRate(CalculatorModel(100, List(PayBack(101, 12)))))
    val state = sender.expectMsgType[APRCalculated]
    val rate = state.getRoundedAPR
    val regex = """\d+(\.\d{1})?""".r
    //regex.f
  }

  test("AprCalculator should calculate correct APR rate using testProbe") {
    val sender = TestProbe()
    val clc = CalculatorModel(100, List(PayBack(101, 12)))
    sender.send(aprCalculator, CalculateRate(clc))
    val state = sender.expectMsgType[APRCalculated]
    assert(clc.calculate(state.getRoundedAPR) < 0.00001)
  }
}
