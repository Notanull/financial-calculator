package com.navneetgupta.domain.actors

import akka.testkit.ImplicitSender
import com.navneetgupta.infrastructure.StopSystemAfterAll
import akka.testkit.TestKit
import org.scalatest.FlatSpecLike
import akka.actor.ActorSystem
import akka.actor.Props
import com.navneetgupta.domain.model.CalculatorModel
import com.navneetgupta.domain.model.PayBack
import com.navneetgupta.domain.events.IRRCalculated
import akka.testkit.TestProbe
import com.navneetgupta.domain.model._
import java.util.Date
import com.navneetgupta.infrastructure.model.FullResult
import org.scalatest.FunSuiteLike

class CalculatorManagerSpec extends TestKit(ActorSystem("calculator-test"))
    with FunSuiteLike
    with ImplicitSender
    with StopSystemAfterAll{

  val calculatorManager = system.actorOf(Props[CalculatorManager], CalculatorManager.Name)
  val dateFormat = "yyyy-MM-dd"
  val formatIncomming = new java.text.SimpleDateFormat(dateFormat)

  val clr = InputModel(51020400, Some(Value(1020400)), List(
        Schedules(1, new Date(formatIncomming.parse("2016-10-20").getTime), 3595000, 1530600),
        Schedules(2, new Date(formatIncomming.parse("2016-11-20").getTime), 3702800, 1422800),
        Schedules(3, new Date(formatIncomming.parse("2016-12-20").getTime), 3813900, 1311700),
        Schedules(4, new Date(formatIncomming.parse("2017-01-20").getTime), 3928300, 1197300),
        Schedules(5, new Date(formatIncomming.parse("2017-02-20").getTime), 4046200, 1079400),
        Schedules(6, new Date(formatIncomming.parse("2017-03-20").getTime), 4167600, 958000),
        Schedules(7, new Date(formatIncomming.parse("2017-04-20").getTime), 4292600, 833000),
        Schedules(8, new Date(formatIncomming.parse("2017-05-20").getTime), 4421400, 704200),
        Schedules(9, new Date(formatIncomming.parse("2017-06-20").getTime), 4554000, 571600),
        Schedules(10, new Date(formatIncomming.parse("2017-07-20").getTime), 4690600, 435000),
        Schedules(11, new Date(formatIncomming.parse("2017-08-20").getTime), 4831400, 294200),
        Schedules(12, new Date(formatIncomming.parse("2017-09-20").getTime), 4976600, 149300),
        ))

  test("CalculatorManager should handle Message CalculateRate using testProbe") {
    val sender = TestProbe()
    sender.send(calculatorManager, clr)
    val state = sender.expectMsgType[FullResult[ResponseModel]]
  }

  test("CalculatorManager should calculate correct APR and IRR using testProbe") {
    val sender = TestProbe()
    sender.send(calculatorManager, clr)
    val state = sender.expectMsgType[FullResult[ResponseModel]]
    val result = state.getOrElse(ResponseModel(0.0,0.0))
    assert(result.apr === 48.3)
    assert(result.irr - 0.0334008783 < 0.000001)
  }

  test("CalculatorManager should calculate correct APR2 and IRR2 using testProbe") {
    val sender = TestProbe()
    val clc =  InputModel(51020400, Some(Value(1020400)), List(
        Schedules(1, new Date(formatIncomming.parse("2016-10-20").getTime), 51020400, 1530600)))

    sender.send(calculatorManager, clc)

    val state = sender.expectMsgType[FullResult[ResponseModel]]
    val result = state.getOrElse(ResponseModel(0.0,0.0))
    assert(result.apr === 81.7)
    assert(result.irr - 0.05102 < 0.000001)
  }

}
