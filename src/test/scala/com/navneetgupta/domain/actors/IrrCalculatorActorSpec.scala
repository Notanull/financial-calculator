package com.navneetgupta.domain.actors

import akka.testkit.TestKit
import akka.actor.ActorSystem
import com.navneetgupta.infrastructure.StopSystemAfterAll
import akka.testkit.ImplicitSender
import org.scalatest.FlatSpecLike
import akka.actor.Props
import akka.testkit.TestProbe
import com.navneetgupta.domain.model._
import com.navneetgupta.domain.events.IRRCalculated
import org.scalatest.FunSuiteLike
import com.navneetgupta.domain.command.CalculateRate

class IrrCalculatorActorSpec extends TestKit(ActorSystem("calculator-test"))
    with FunSuiteLike
    with ImplicitSender
    with StopSystemAfterAll {

  import CalculatorModel._
  import AlgebraInstances._

  val irrCalculator = system.actorOf(Props[IrrCalculatorActor], "irrCalculator")

  test("IrrCalculator should handle Command CalculateIRR using testProbe") {
    val sender = TestProbe()
    sender.send(irrCalculator, CalculateRate(CalculatorModel(100, List(PayBack(101, 12)))))
    val state = sender.expectMsgType[IRRCalculated]
  }

  test("IrrCalculator should calculate correct IRR rate using testProbe") {
    val sender = TestProbe()
    val clc = CalculatorModel(100, List(PayBack(101, 12)))
    sender.send(irrCalculator, CalculateRate(clc))
    val state = sender.expectMsgType[IRRCalculated]
    assert(clc.calculate(state.irr) < 0.00001)
  }

}
